package ru.tsk.vkorenygin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.command.AbstractTaskCommand;
import ru.tsk.vkorenygin.tm.entity.Task;
import ru.tsk.vkorenygin.tm.enumerated.Role;
import ru.tsk.vkorenygin.tm.enumerated.Sort;
import ru.tsk.vkorenygin.tm.util.EnumerationUtil;
import ru.tsk.vkorenygin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskListCommand extends AbstractTaskCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "task-list";
    }

    @Override
    public @Nullable String description() {
        return "show task list";
    }

    @Override
    public @NotNull Role @NotNull [] roles() {
        return new Role[] {Role.USER};
    }

    @Override
    public void execute() {
        @NotNull final String currentUserId = serviceLocator.getAuthService().getUserId();
        System.out.println("[LIST TASKS]");
        System.out.println("ENTER SORT: ");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sort = TerminalUtil.nextLine();
        @Nullable Sort sortType = EnumerationUtil.parseSort(sort);

        @Nullable List<Task> tasks;
        tasks = sortType == null ?
                serviceLocator.getTaskService().findAll(currentUserId) :
                serviceLocator.getTaskService().findAll(sortType.getComparator(), currentUserId);

        int index = 1;
        for (@NotNull final Task task : tasks) {
            @NotNull final String projectStatus = task.getStatus().getDisplayName();
            System.out.println(index + ". " + task + " (" + projectStatus + ")");
            index++;
        }
    }

}
