package ru.tsk.vkorenygin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.api.repository.IOwnerRepository;
import ru.tsk.vkorenygin.tm.api.service.IOwnerService;
import ru.tsk.vkorenygin.tm.entity.AbstractOwnerEntity;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;
import ru.tsk.vkorenygin.tm.exception.entity.EntityNotFoundException;
import ru.tsk.vkorenygin.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.vkorenygin.tm.exception.system.IncorrectIndexException;
import ru.tsk.vkorenygin.tm.exception.user.AccessDeniedException;
import ru.tsk.vkorenygin.tm.util.DataUtil;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity> implements IOwnerService<E> {

    @NotNull
    protected IOwnerRepository<E> ownerRepository;

    public AbstractOwnerService(IOwnerRepository<E> repository) {
        this.ownerRepository = repository;
    }

    @Override
    public @NotNull E add(final @Nullable E entity) throws AbstractException {
        if (entity == null) throw new EntityNotFoundException();
        ownerRepository.add(entity);
        return entity;
    }

    @Override
    public boolean existsById(final @Nullable String id, final @Nullable String userId) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) return false;
        return ownerRepository.existsById(id, userId);
    }

    @Override
    public boolean existsByIndex(final int index, final @Nullable String userId) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (index < 0) return false;
        return ownerRepository.existsByIndex(index, userId);
    }

    @Override
    public @NotNull List<E> findAll(final @Nullable String userId) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        return ownerRepository.findAll(userId);
    }

    @Override
    public @NotNull List<E> findAll(final @Nullable Comparator<E> comparator, final @Nullable String userId) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (comparator == null) return Collections.emptyList();
        return ownerRepository.findAll(comparator, userId);
    }

    @Override
    public @NotNull Optional<E> findById(final @Nullable String id, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        return ownerRepository.findById(id, userId);
    }

    @Override
    public @NotNull Optional<E> findByIndex(final int index, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (index < 0) throw new IncorrectIndexException();
        return ownerRepository.findByIndex(index, userId);
    }

    @Override
    public @NotNull Optional<E> removeById(final @Nullable String id, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        return ownerRepository.removeById(id, userId);
    }

    @Override
    public @NotNull Optional<E> removeByIndex(final int index, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (index < 0) throw new IncorrectIndexException();
        return ownerRepository.removeByIndex(index, userId);
    }

    @Override
    public @NotNull Optional<E> remove(final @Nullable E entity, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (entity == null) throw new ProjectNotFoundException();
        return ownerRepository.remove(entity, userId);
    }

    @Override
    public void clear(@Nullable final String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        ownerRepository.clear(userId);
    }

}
