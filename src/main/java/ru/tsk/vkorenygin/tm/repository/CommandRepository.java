package ru.tsk.vkorenygin.tm.repository;

import lombok.SneakyThrows;
import org.reflections.Reflections;
import ru.tsk.vkorenygin.tm.api.repository.ICommandRepository;
import ru.tsk.vkorenygin.tm.command.AbstractCommand;
import ru.tsk.vkorenygin.tm.util.DataUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Modifier;
import java.util.*;

public class CommandRepository implements ICommandRepository {

    @NotNull
    private final List<AbstractCommand> commandList = new ArrayList<>();

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    {
        initCommands();
    }

    @SneakyThrows
    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.tsk.vkorenygin.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(ru.tsk.vkorenygin.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            commandList.add(clazz.newInstance());
        }
        commandList.sort(Comparator.comparing(AbstractCommand::name));
    }

    @Override
    public @NotNull List<AbstractCommand> getCommandList() {
        return commandList;
    }


    @Override
    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Override
    @NotNull
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @Override
    @NotNull
    public Collection<String> getCommandNames() {
        @NotNull final List<String> result = new ArrayList<>();
        for (@NotNull final AbstractCommand command : commands.values()) {
            @NotNull final String name = command.name();
            if (DataUtil.isEmpty(name))
                continue;
            result.add(name);
        }
        return result;
    }

    @Override
    @NotNull
    public Collection<String> getCommandArgs() {
        @NotNull final List<String> result = new ArrayList<>();
        for (@NotNull final AbstractCommand command : arguments.values()) {
            @NotNull final String name = command.name();
            if (DataUtil.isEmpty(name))
                continue;
            result.add(name);
        }
        return result;
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByName(@NotNull final String name) {
        return commands.get(name);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByArg(@NotNull final String name) {
        return arguments.get(name);
    }

    @Override
    public void add(final @NotNull AbstractCommand command) {
        @Nullable final String arg = command.arg();
        @Nullable final String name = command.name();
        if (!DataUtil.isEmpty(arg)) arguments.put(arg, command);
        if (!DataUtil.isEmpty(name)) commands.put(name, command);
    }

}
