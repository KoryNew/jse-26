package ru.tsk.vkorenygin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.entity.User;
import ru.tsk.vkorenygin.tm.enumerated.Role;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;

import java.util.Optional;

public interface IUserService extends IService<User> {

    boolean isLoginExists(@Nullable final String login) throws AbstractException;

    boolean isEmailExists(@Nullable final String email) throws AbstractException;

    @NotNull
    User create(@Nullable final String login, @Nullable final String password) throws AbstractException;

    @NotNull
    User create(@Nullable final String login,
                @Nullable final String password,
                @Nullable final String email) throws AbstractException;

    @NotNull
    User create(@Nullable final String login,
                @Nullable final String password,
                @Nullable final Role role) throws AbstractException;

    @NotNull
    Optional<User> findByLogin(@Nullable final String login) throws AbstractException;

    @NotNull
    Optional<User> findByEmail(@Nullable final String email) throws AbstractException;

    @NotNull
    Optional<User> removeByLogin(@Nullable final String login) throws AbstractException;

    @NotNull
    User setPassword(@Nullable final String userId, @Nullable final String password) throws AbstractException;

    @NotNull
    User updateUser(@Nullable final String userId,
                    @Nullable final String firstName,
                    @Nullable final String lastName,
                    @Nullable final String middleName) throws EmptyIdException;

    @NotNull
    Optional<User> lockByLogin(@Nullable String login) throws AbstractException;

    @NotNull
    Optional<User> unlockByLogin(@Nullable String login) throws AbstractException;

}
