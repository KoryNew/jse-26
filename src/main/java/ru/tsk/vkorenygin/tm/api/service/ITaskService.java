package ru.tsk.vkorenygin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.entity.Task;
import ru.tsk.vkorenygin.tm.enumerated.Status;

import java.util.Optional;

public interface ITaskService extends IOwnerService<Task> {

    void create(@Nullable final String name, @Nullable final String userId);

    void create(@Nullable final String name, @Nullable final String description, @Nullable final String userId);

    @NotNull
    Optional<Task> findByName(@Nullable final String name, @Nullable final String userId);

    @NotNull
    Task changeStatusById(@Nullable final String id, @Nullable final Status status, @Nullable final String userId);

    @NotNull
    Task changeStatusByName(@Nullable final String name, @Nullable final Status status, @Nullable final String userId);

    @NotNull
    Task changeStatusByIndex(@Nullable final Integer index, @Nullable final Status status, @Nullable final String userId);

    @NotNull
    Task startById(@Nullable final String id, @Nullable final String userId);

    @NotNull
    Task startByIndex(@Nullable final Integer index, @Nullable final String userId);

    @NotNull
    Task startByName(@Nullable final String name, @Nullable final String userId);

    @NotNull
    Task finishById(@Nullable final String id, @Nullable final String userId);

    @NotNull
    Task finishByIndex(@Nullable final Integer index, @Nullable final String userId);

    @NotNull
    Task finishByName(@Nullable final String name, @Nullable final String userId);

    @NotNull
    Task updateById(@Nullable final String id,
                    @Nullable final String name,
                    @Nullable final String description,
                    @Nullable final String userId);

    @NotNull
    Task updateByIndex(@Nullable final Integer index,
                       @Nullable final String name,
                       @Nullable final String description,
                       @Nullable final String userId);

    @NotNull
    Optional<Task> removeByName(@Nullable final String name, @Nullable final String userId);

}
