package ru.tsk.vkorenygin.tm.exception.system;

import ru.tsk.vkorenygin.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException() {
        super("Error! Command not found. Use ``help`` to display the list of available commands");
    }

}
