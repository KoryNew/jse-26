package ru.tsk.vkorenygin.tm.comparator;

import ru.tsk.vkorenygin.tm.api.entity.IHasStatus;

import java.util.Comparator;

public class ComparatorByStatus implements Comparator<IHasStatus> {

    public static final ComparatorByStatus INSTANCE = new ComparatorByStatus();

    private ComparatorByStatus() {
    }

    @Override
    public int compare(IHasStatus o1, IHasStatus o2) {
        return o1.getStatus().getPriority() - o2.getStatus().getPriority();
    }

}
